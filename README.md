# README #

Projecto final para cadeira Internet das Coisas (2019/2020)

Pasta meta_3 deve estar numa directoria ao mesmo nivel do SDK contiki

### EX:
    projecto
        |_ contiki
        |_ meta_3
            |_ Arduino
                |_ Modulo sensores
                    |_ ...
            |_ Zolertia
                |_ Modulo estufa
                    |_ ...
                |_ Modulo sub-estufa
                    |_ ...
            |_ ...


## Estrutura de projecto

A pasta Arduino contém todos os ficheiros necessários para programar e testar os módulos de sensores

A pasta Zolertia contém todos os ficheiros relativos aos módulos zolertia (módulo de agregação para arduinos e módulo de agregação para estufa)

## Descrição de projecto

### Sensores
    - Sensor de temperatura e humidade (DHT22)
    - Sensor de humidade do solo
    - Sensor de luminusidade

### Módulos
    - Arduino(s)
    - Zolertia (agregador de arduinos)
    - Zolertia (agregador de estufa)

### Estrutura

    Servidor
    |_ Zolertia (agregador de estufa)
        |_ Zolertia (agregador de arduinos)
            |_ Arduino
                |_ Sensor DHT22
                |_ Sensor humidade de solo
                |_ Sensor luminusidade
            |_ Arduino
                |_ Sensor DHT22
                |_ Sensor humidade de solo
                |_ Sensor luminusidade
            |_ Arduino
                |_ Sensor DHT22
                |_ Sensor humidade de solo
                |_ Sensor luminusidade

        |_ Zolertia (agregador de arduinos)
            |_ Arduino
                |_ Sensor DHT22
                |_ Sensor humidade de solo
                |_ Sensor luminusidade
            |_ Arduino
                |_ Sensor DHT22
                |_ Sensor humidade de solo
                |_ Sensor luminusidade

    |_ Zolertia (agregador de estufa)
        |_ Zolertia (agregador de arduinos)
            |_ Arduino
                |_ Sensor DHT22
                |_ Sensor humidade de solo
                |_ Sensor luminusidade
    |_ ...


### Estrutura da simulação para entrega

    Servidor
        |_ Zolertia (agregador de estufa)
            |_ Zolertia (agregador de arduinos)
                |_ Arduino
                    |_ Sensor DHT22
                    |_ Sensor humidade de solo
                    |_ Sensor luminusidade
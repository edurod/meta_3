/**
  Licensed to the Apache Software Foundation (ASF) under one
  or more contributor license agreements.  See the NOTICE file
  distributed with this work for additional information
  regarding copyright ownership.  The ASF licenses this file
  to you under the Apache License, Version 2.0 (the
  "License"); you may not use this file except in compliance
  with the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an
  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  KIND, either express or implied.  See the License for the
  specific language governing permissions and limitations
  under the License.

 *******************************************************************************
 * @file main_core.c
 * @author Ânderson Ignacio da Silva
 * @date 19 Ago 2016
 * @brief Main code to test MQTT-SN on Contiki-OS
 * @see http://www.aignacio.com
 * @license This project is licensed by APACHE 2.0.
 */

#define SIMULATION 1
#define BASE_TOPIC "IC_LDA/Coimbra"

#include "contiki.h"
#include "lib/random.h"
#include "clock.h"
#include "sys/ctimer.h"
#include "net/ip/uip.h"
#include "net/ipv6/uip-ds6.h"
#include "mqtt_sn.h"
#include "dev/leds.h"
#include "net/rime/rime.h"
#include "net/ip/uip.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#if SIMULATION == 0
  #include "contiki/platform/zoul/dev/dht22.h"
  #include "contiki/platform/zoul/dev/adc-sensors.h"

  #define ADC_PIN 5
#endif


static uint16_t udp_port = 1884;
static uint16_t keep_alive = 5;
static uint16_t broker_address[] = {0xaaaa, 0, 0, 0, 0, 0, 0, 0x1};
static struct   etimer time_poll;
// static uint16_t tick_process = 0;
static char     device_id[17];
static char     topic_base[] = BASE_TOPIC;
static char     pub_message[100];
static char     topic_sub[50];
static char     topic_pub[50];
int16_t temperature, humidity;
// static char     *will_topic = "/6lowpan_node/offline";
// static char     *will_message = "O dispositivo esta offline";
// This topics will run so much faster than others

mqtt_sn_con_t mqtt_sn_connection;

void mqtt_sn_callback(char *topic, char *message){

  if (strcmp(message,"Force"))
  {
    #if SIMULATION == 1
        snprintf(pub_message,100,"{\"ID\":\"%s\",\"Timestamp\":\"%ld\",\"Temp\":\"25.%02u\",\"Hum\":\"8%01u\",\"Lum\":\"%ld\"}",
                  &device_id[sizeof(device_id)-4],
                  clock_seconds(),
                  (unsigned int)rand()%100,
                  (unsigned int)rand()%100,
                  (clock_seconds()*100)%100);
        mqtt_sn_pub(topic_pub,pub_message,true,0);
    #else
        int16_t aux_1, aux_2;
        if(dht22_read_all(&aux_1, &aux_2) != DHT22_ERROR) {
          temperature = aux_1;
          humidity = aux_2;
        }
        snprintf(pub_message,100,"{\"ID\":\"%s\",\"Timestamp\":\"%ld\",\"Temp\":\"%02d.%02d\",\"Hum\":\"%02d.%02d\",\"Lum\":\"%d\"}",
                  &device_id[sizeof(device_id)-4],
                  clock_seconds()*1000,
                  temperature / 10, temperature % 10,
                  humidity / 10, humidity % 10,
                  adc_sensors.value(ANALOG_GROVE_LIGHT));
        mqtt_sn_pub(topic_pub,pub_message,true,0);
    #endif
  }
  // else if (strcmp(message,"Temp"))
  // {
  //   /* code */
  // }
  // else if (strcmp(message,"Hum"))
  // {
  //   /* code */
  // }
  // else if (strcmp(message,"Lum"))
  // {
  //   /* code */
  // }
}

void init_broker(void){
  
  sprintf(device_id,"%02X%02X%02X%02X%02X%02X%02X%02X",
          linkaddr_node_addr.u8[0],linkaddr_node_addr.u8[1],
          linkaddr_node_addr.u8[2],linkaddr_node_addr.u8[3],
          linkaddr_node_addr.u8[4],linkaddr_node_addr.u8[5],
          linkaddr_node_addr.u8[6],linkaddr_node_addr.u8[7]);
  sprintf(topic_sub,"%s/%02X%02X/Cmd",
          topic_base,
          linkaddr_node_addr.u8[6],linkaddr_node_addr.u8[7]);
  sprintf(topic_pub,"%s/Data", &topic_base);

  mqtt_sn_connection.client_id     = device_id;
  mqtt_sn_connection.udp_port      = udp_port;
  mqtt_sn_connection.ipv6_broker   = broker_address;
  mqtt_sn_connection.keep_alive    = keep_alive;
  //mqtt_sn_connection.will_topic    = will_topic;   // Configure as 0x00 if you don't want to use
  //mqtt_sn_connection.will_message  = will_message; // Configure as 0x00 if you don't want to use
  mqtt_sn_connection.will_topic    = 0x00;
  mqtt_sn_connection.will_message  = 0x00;

  mqtt_sn_init();   // Inicializa alocação de eventos e a principal PROCESS_THREAD do MQTT-SN

  char *all_topics[2];
  all_topics[0] = topic_pub;
  all_topics[1] = topic_sub;

  mqtt_sn_create_sck(mqtt_sn_connection,
                     all_topics,
                     ss(all_topics),
                     mqtt_sn_callback);
  mqtt_sn_sub(topic_sub,0);
}

/*---------------------------------------------------------------------------*/
PROCESS(init_system_process, "Módulo Estufa");
AUTOSTART_PROCESSES(&init_system_process);
/*---------------------------------------------------------------------------*/

PROCESS_THREAD(init_system_process, ev, data) {
  PROCESS_BEGIN();

  #if SIMULATION == 1
      etimer_set(&time_poll, CLOCK_SECOND*2);
  #else
      SENSORS_ACTIVATE(dht22);
      adc_sensors.configure(ANALOG_GROVE_LIGHT, ADC_PIN);
      etimer_set(&time_poll, CLOCK_SECOND*60);
  #endif

  init_broker();

  sprintf(pub_message,"subscribing to:%s",topic_sub);
  mqtt_sn_pub(topic_pub,pub_message,true,0);

  while(1) {
      PROCESS_WAIT_EVENT();

  #if SIMULATION == 1
      snprintf(pub_message,100,"{\"ID\":\"%s\",\"Timestamp\":\"%ld\",\"Temp\":\"25.%02u\",\"Hum\":\"8%01u\",\"Lum\":\"%ld\"}",
                &device_id[sizeof(device_id)-4],
                clock_seconds()*1000,
                (unsigned int)rand()%100,
                (unsigned int)rand()%100,
                (clock_seconds()*100)%100);
      mqtt_sn_pub(topic_pub,pub_message,true,0);
  #else
      int16_t aux_1, aux_2;
      if(dht22_read_all(&aux_1, &aux_2) != DHT22_ERROR) {
        temperature = aux_1;
        humidity = aux_2;
      }
      snprintf(pub_message,100,"{\"ID\":\"%s\",\"Timestamp\":\"%ld\",\"Temp\":\"%02d.%02d\",\"Hum\":\"%02d.%02d\",\"Lum\":\"%d\"}",
                &device_id[sizeof(device_id)-4],
                clock_seconds()*1000,
                temperature / 10, temperature % 10,
                humidity / 10, humidity % 10,
                adc_sensors.value(ANALOG_GROVE_LIGHT));
      mqtt_sn_pub(topic_pub,pub_message,true,0);
  #endif

      if (etimer_expired(&time_poll))
        etimer_reset(&time_poll);
  }

  PROCESS_END();
}
